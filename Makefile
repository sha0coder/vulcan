
all:
	g++ -c iface.cpp -std=c++11 -static -s #-std=c++0x  
	g++ -c forkpool.cpp -std=c++11 -static -s 
	g++ vulcan.cpp iface.o forkpool.o -o vulcan -std=c++11 -static -s
	strip vulcan

clean:
	rm -f iface.o forkpool.o vulcan
	
