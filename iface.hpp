#include <net/if.h> 
#include <net/ethernet.h>
#include <net/if.h> // struct ifreq
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <arpa/inet.h>

class Iface {
private:
	bool debug = false;
	int rawsock;
public:
	struct ether_addr mac;
	unsigned int mtu;
	unsigned long broadcast;
	struct sockaddr_in ip;

	void setDebug();
	bool set(const char *);
	int getSocket();
	void end();

};