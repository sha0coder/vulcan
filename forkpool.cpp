#include <unistd.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <ctime>
#include "forkpool.hpp"
#include <sys/timeb.h>


bool ForkPool::isRunning() {
    //std::cout << this->getSeconds() << " " << this->timeout << "\n";
    return (this->running && this->getSeconds() < this->timeout);
}

int ForkPool::getNodes() {
    return this->nodes;
}

void ForkPool::destroy() {
    this->stop();
}

int ForkPool::getSeconds() {
    struct timeb tp;
    ftime(&tp);
    return tp.time;
}

int ForkPool::getPid() {
    return this->pid;
}

void ForkPool::start(int nodes, int timeout) {
    int i;
    this->nodes = nodes;
    this->running = true;
    this->timeout = this->getSeconds() + timeout;

    for (i=0; i<this->nodes; i++) {
        this->pid = fork();
        if (this->pid != 0)
            break;
        //this->pids.push_back(fork());
    }
}

void ForkPool::stop() {
    this->running = false;
    if (this->pid != 0)
        exit(1);
}

int test(void) {
    int i;
    ForkPool pool;

    pool.start(5, 5);

    while(pool.isRunning()) 
        std::cout << "test\n";

    std::cout << pool.getPid();


    std::cout << pool.getPid() << " end1\n";
    pool.stop();


    std::cout << pool.getPid() << " end2\n";
    



    

}