#include <vector>
#include <chrono>

class ForkPool {
private:
    int pid;
    int timeout;
    std::vector<int> pids;
    int nodes;
    bool running;

protected:
    int getSeconds();

public:
    int getNodes();
    int getPid();
    void start(int nodes, int timeout);
    bool isRunning();
    void stop();
    void destroy();
};