#include <net/ethernet.h>

#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>

#include "iface.hpp"
#include "rawip.hpp"

#define DATA_SZ 100

struct rawtcp {
	struct ipheader ip;
	struct tcpheader tcp;
	char data[DATA_SZ];
};

struct rawudp {
	struct ipheader ip;
	struct udpheader udp;
	char data[DATA_SZ];
};

class Vulcan {
private:
	Iface iface;
	int rawsock; //TODO: socketpool
	struct sockaddr_in target;

public:
	bool init(const char *);
	bool setTarget(const char *,unsigned int);
	char *genRandom(int sz);
	void randTCP(int times);
	void randUDP(int times);
	void destroy();
};


