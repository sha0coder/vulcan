#include "iface.hpp"
#include <string.h>
#include <iostream>
#include <unistd.h>



void Iface::setDebug() {
	debug = true;
}

int Iface::getSocket() {
	return rawsock;
}

void Iface::end() {
    close(rawsock);
}

bool Iface::set(const char *dev) {

	struct ifreq ifr;

	if ((rawsock = socket(AF_INET, SOCK_RAW, IPPROTO_RAW))<0) {
		if (debug)
			std::cout << "cant create raw sockets, are you root?\n";
		return false;
	}


    // HW Addr
    memset(&ifr,0,sizeof(ifr));
    strncpy(ifr.ifr_name, dev, sizeof(ifr.ifr_name));
    if (ioctl(rawsock, SIOCGIFHWADDR, &ifr) < 0) 
        return false;
    memcpy(&mac, &ifr.ifr_hwaddr.sa_data, sizeof(struct ether_addr));

    if (debug)
    	std::cout << "mac read\n";

    // IP Addr
    memset(&ifr,0,sizeof(ifr));
    strncpy(ifr.ifr_name, dev, sizeof (ifr.ifr_name));
    if (ioctl(rawsock, SIOCGIFADDR, &ifr) < 0)
        return false;
    memcpy(&ip, &ifr.ifr_addr, sizeof(struct sockaddr_in));

    if (debug)
    	std::cout << "ip read\n";

    // MTU
    memset(&ifr,0,sizeof(ifr));
    strncpy(ifr.ifr_name, dev, sizeof (ifr.ifr_name));
    if (ioctl(rawsock, SIOCGIFMTU, &ifr) < 0)
        return false;
    mtu=ifr.ifr_mtu;

    if (debug)
    	std::cout << "mtu read\n";

    // broadcast
    memset(&ifr,0,sizeof(ifr));
    strncpy(ifr.ifr_name, dev, sizeof (ifr.ifr_name));
    if (ioctl(rawsock, SIOCGIFBRDADDR, &ifr) < 0)
        return false;
    memcpy(&broadcast, &(*(struct sockaddr_in *)&ifr.ifr_addr).sin_addr.s_addr, sizeof(unsigned long));

	if (debug)
    	std::cout << "broadcast read\n";

	return true;
}
