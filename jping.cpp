// Created by sha0 on 10/06/15.

#include "packet.h"


void usage() {
    cout << "jping Usage: " << endl;
    cout << " -T                (tcp)" << endl;
    cout << " -U                (udp)" << endl;
    cout << " -i                (icmp uninmplemented by now)" << endl;
    cout << " -h 192.168.1.1    (remote host)" << endl;
    cout << " -p 80             (remote port)" << endl;
    cout << " -d eth0           (interface)" << endl;
    cout << " -f SA             (flags Syn and Ack, or any combination)" << endl;
    cout << " -s b10            (speed limit b bytes, k kbytes, m megas)" << endl;
    cout << " -t 1000           (timeout in seconds)" << endl;
    cout << " -c 100            (packet count, use -t or -c)" << endl << endl;
    cout << "example: " << endl;
    cout << "  jping -T -h 192.168.1.1 -p 80 -f SARUPF -s m1 -t 100" << endl << endl;
    exit(1);
}

int main(int argc, char **argv) {
    char *host;
    char *maxSpeed;
    int port;
    float timeout = -1;
    float pktCount = -1;
    int srcPort = 53;
    char c;
    opterr = 0;

    Packet pkt = Packet();

    if (argc < 5)
        usage();

    while ((c = getopt(argc, argv, "TUd:h:p:f:s:t:c:")) != -1) {
        switch (c) {
            case 'T': pkt.setTCP(); break;
            case 'U': pkt.setUDP(); break;
            case 'h': host = optarg; break;
            case 'p': port = atoi(optarg); break;
            case 'd': pkt.setDevice(optarg); break;
            case 'f': pkt.setTCPFlags(optarg); break;
            case 's': maxSpeed = optarg; break;
            case 't': timeout = atof(optarg); break;
            case 'c': pktCount = atof(optarg); break;
            case '?': cout << "Bad option" << endl; break;
            default: usage();
        }
    }

    pkt.setTarget(host, port);
    pkt.setData("TEST");
    pkt.setSourcePort(srcPort);

    pkt.send();

}
