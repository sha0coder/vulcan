
#include <net/if.h>
#include <netinet/in.h>

struct ipheader {
    u_int8_t        ihl:4,          /* header length */
                    version:4;      /* version */
    u_int8_t        tos;            /* type of service */
    u_int16_t       tot_len;        /* total length */
    u_int16_t       id;             /* identification */
    u_int16_t       off;            /* fragment offset field */
    u_int8_t        ttl;            /* time to live */
    u_int8_t        protocol;       /* protocol */
    u_int16_t       check;          /* checksum */
    struct in_addr  saddr;
    struct in_addr  daddr;  	/* source and dest address */
};

struct udpheader {
    u_int16_t	sport;
    u_int16_t	dport;
    u_int16_t	length;
    u_int16_t	checksum;
};

struct tcpheader {
   	u_int16_t sport;         /* source port */
    u_int16_t dport;         /* destination port */
    u_int32_t seq;             /* sequence number */
    u_int32_t ack;             /* acknowledgement number */
#if __BYTE_ORDER == __LITTLE_ENDIAN
    u_int8_t x2:4;           /* (unused) */
    u_int8_t off:4;          /* data offset */
#endif
#if __BYTE_ORDER == __BIG_ENDIAN
    u_int8_t off:4;          /* data offset */
    u_int8_t x2:4;           /* (unused) */
#endif
    u_int8_t flags;
#define TH_FIN  0x01
#define TH_SYN  0x02
#define TH_RST  0x04
#define TH_PUSH 0x08
#define TH_ACK  0x10
#define TH_URG  0x20
    u_int16_t win;           /* window */
    u_int16_t sum;           /* checksum */
    u_int16_t urg;           /* urgent pointer */
};



unsigned short csum(unsigned short *buf, int len) {
    unsigned long sum;
    for(sum=0; len>0; len--)
            sum += *buf++;
    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);
    return (unsigned short)(~sum);
}