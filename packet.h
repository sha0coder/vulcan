//
// Created by sha0 on 10/06/15.
//

#include <net/ethernet.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include <unistd.h>
#include <string.h>

#include "iface.hpp"
#include "rawip.hpp"

using namespace std;


#define DATA_SZ 100


class Packet {

private:
    bool destroyed;
    bool isUdp;

    struct rawtcp {
        struct ipheader ip;
        struct tcpheader tcp;
        char data[DATA_SZ];
    };

    struct rawudp {
        struct ipheader ip;
        struct udpheader udp;
        char data[DATA_SZ];
    };

    int sock;
    Iface iface;
    struct sockaddr_in target;


public:
    struct rawtcp tcp_pkt;
    struct rawudp udp_pkt;


    Packet() {
    }

    ~Packet() {
        Destroy();
    }

    void Destroy() {
        if (!destroyed)
            close(sock);
        destroyed = true;
    }

    void setTCP() {
        isUdp = false;
        sock = socket(PF_INET, SOCK_RAW, IPPROTO_TCP);
        customIPProto();
        buildTCP();
        destroyed = false;
    }

    void setUDP() {
        isUdp = true;
        sock = socket(PF_INET, SOCK_RAW, IPPROTO_UDP);
        customIPProto();
        buildUDP();
        destroyed = false;
    }

    void setDevice(char *dev) {
        if (!iface.set(dev)) {
            cout << "Inproper device" << endl;
            exit(1);
        }
    }

    bool setTarget(const char *host, unsigned int port) {
        struct hostent *hostinfo;

        if ((hostinfo = gethostbyname(host)) == NULL)
            return false;

        target.sin_family = AF_INET;
        target.sin_port = htons(port);
        target.sin_addr = *(struct in_addr *) (hostinfo->h_addr);

        cout << "src ip: " << iface.ip.sin_addr << endl;

        tcp_pkt.ip.saddr = iface.ip.sin_addr;
        tcp_pkt.ip.daddr = target.sin_addr;
        tcp_pkt.tcp.dport = target.sin_port;
        udp_pkt.ip.saddr = iface.ip.sin_addr;
        udp_pkt.ip.daddr = target.sin_addr;
        udp_pkt.udp.dport = target.sin_port;

        return true;
    }

    void setTCPFlags(const char *flags) {
        tcp_pkt.tcp.flags = 0;
        for (int i=0; i<strlen(flags); i++) {
            switch(flags[i]) {
                case 'S': tcp_pkt.tcp.flags |=  TH_SYN;  break;
                case 'A': tcp_pkt.tcp.flags |=  TH_ACK;  break;
                case 'P': tcp_pkt.tcp.flags |=  TH_PUSH; break;
                case 'R': tcp_pkt.tcp.flags |=  TH_RST;  break;
                case 'U': tcp_pkt.tcp.flags |=  TH_URG;  break;
                case 'F': tcp_pkt.tcp.flags |=  TH_FIN;  break;
            }
        }
    }

    void setSourcePort(u_int16_t port) {
        tcp_pkt.tcp.sport = port;
        udp_pkt.udp.sport = port;
    }

    void buildTCP() {
        memset(&tcp_pkt, 0, sizeof(struct rawtcp));

        tcp_pkt.ip.ihl = 5;
        tcp_pkt.ip.version = 4;
        tcp_pkt.ip.tos = 1;
        tcp_pkt.ip.tot_len = sizeof(struct ipheader) + sizeof(struct tcpheader);
        tcp_pkt.ip.id = 0;
        tcp_pkt.ip.off = 0; //no modificar
        tcp_pkt.ip.ttl = 255;
        tcp_pkt.ip.protocol = 6; // TCP
        tcp_pkt.ip.check = 0; // Done by kernel

        //tcp_pkt.ip.saddr = iface.ip.sin_addr;
        //tcp_pkt.ip.daddr = target.sin_addr;

        //tcp_pkt.tcp.sport = (u_int16_t)random[3];
        //tcp_pkt.tcp.dport = target.sin_port;

        tcp_pkt.tcp.seq = htonl(rand());
        tcp_pkt.tcp.ack = htonl(0);
        tcp_pkt.tcp.x2 = 0;
        tcp_pkt.tcp.off =  rand() * (sizeof(struct tcpheader));

        //tcp_pkt.tcp.flags = 0;

        tcp_pkt.tcp.win = htons(16350); //htons(32767);
        tcp_pkt.tcp.sum = 0; // Done by kernel
        tcp_pkt.tcp.urg = 0;
    }

    void buildUDP() {
        memset(&udp_pkt, 0, sizeof(struct rawudp));
    }

    void setData(const char *data) {
        size_t sz = DATA_SZ;
        if (strlen(data)<sz)
            sz = strlen(data);

        memcpy(&tcp_pkt.data, data, DATA_SZ);
        memcpy(&udp_pkt.data, data, DATA_SZ);
    }


    void customIPProto() {
        int one = 1;
        const int *val = &one;
        if (setsockopt(sock, IPPROTO_IP, IP_HDRINCL, val, sizeof(one))<0) {
            cout << "root privileges are needed for raw sockets" << endl;
            exit(1);
            //perror("setsockopt");
            return;
        }
    }

    void send() {
        int sz;

        if (isUdp)
            sz = sendto(sock, &udp_pkt, sizeof(udp_pkt), 0, (struct sockaddr *)&target, sizeof(struct sockaddr));
        else
            sz = sendto(sock, &tcp_pkt, sizeof(tcp_pkt), 0, (struct sockaddr *)&target, sizeof(struct sockaddr));

        if (sz < 0)
            perror("sendto");
    }

};